package sample;

import java.sql.*;

/**
 * Creat per David Torralbo el 2/4/18.
 * https://bitbucket.org/torralbo/
 */
class Operacions {

    static boolean iniciaSessio(String usuari, String pass) throws SQLException {

        boolean connectat = false;

        Connexio con = new Connexio();

        String selectSQL = "SELECT usuari FROM usuaris WHERE usuari = ? AND pwd = ?" ;
        PreparedStatement pstmt = con.connect().prepareStatement(selectSQL);
        pstmt.setString(1, usuari);
        pstmt.setString(2, pass);

        ResultSet rs = pstmt.executeQuery();

        if(rs.next()){
            connectat = true;
        }

        return connectat;
    }

}