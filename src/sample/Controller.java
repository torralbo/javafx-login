package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.awt.event.MouseEvent;
import java.sql.SQLException;

public class Controller {

    @FXML
    private TextField usuari;

    @FXML
    private PasswordField contrasenya;

    @FXML
    private Button btnlogin;

    @FXML
    private Label labelevent;

    @FXML
    public void iniciaSessio(ActionEvent actionEvent) throws SQLException {

        if(Operacions.iniciaSessio(usuari.getText(), contrasenya.getText())){
            labelevent.setText("Benvingut, " + usuari.getText());
        } else {
            labelevent.setText("Prova-ho una altra vegada.");
        }

    }
}
